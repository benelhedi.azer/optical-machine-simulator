# FMT imaging scanner simulator

![Plot picture](/om.png)
## Description
#### Goal
FMT medical imaging scanners contain a couple of optical components, like mirrors, filters, lenses and detectors. Each of those components has some scalar properties like position, angle, focal length, etc.

To ensure that the scanner is as efficient and powerfull as possible we need to design it with the best properties. For example if we take a mirror and slightly change its position, then the quality of the generated picture at the end by the detector will change to either better or worse. Our job is to find the best, not only mirror position but every property of every component of any scanner design provided by the user. No matter how complicated it is.

#### Evaluating scanners: objective function
To achieve our product's goal we need first to evaluate every medical scanner or as we call it *optical machine* to decide whether it is a good/efficient enough or not. We think of this evaluation as a simple mathematical function *(objective function)* that takes all properties of all components in an *optical machine* and returns a simple scalar that we would like to maximize.

This *objective function* is not realy explicitly defined as a mathematical function. It's actually quite more compilcated than that and it involves pretty much every part of the software we are building. Nevertheless, it is very clear and straight forward to understand. 

#### The simulator: moving photons from start to end
So to evaluate the objective function we start with an *optical machine* that has a lot of *optical components* with different properties. All this data should be read from an XML file provided by the user. Then we use a set of lightning points with different position. Every lightning point will emmit (generate) a couple of *photons* that will go in random directions. All the lightning points data should be read from a CSV file.
After that, the hard work will start. The simulator will go through all *photons* and move them in their own direction (just like what would happen in real life) until they hit the first or very next *optical component*. Then that component will decide in which direction to send the *photon*. Finally, the same sequence of event will repeat itself until the photons either reach the detector which **must be** the final component in the *optical machine* or go out of track and get lost.

#### Objective function again
Now that we have the *photons* in the *detector* we can generate a picture that the detector would give in real life. This picture should have a lot of qualities like brightness and sharpness, etc. All those values could be summed up together into one scalar which is the value we are looking for. The value of the *objective function*. Quite a long journey.

#### The Optimizer
That's actually not the end yet. We still have a very long road to go. What we did until now is just calculating the *objective function* value for the initial properties of the *optical machine*.
But our ultimate goal is to find the exact best properities. To do so we are going to use an *optimizer* that will search for those very important values.

Now we are talking purly math. An *optimizer* in basically nothing but an implementation of a mathematical optimization algorithm. Such kind of algorithms optimize mathematical functions but what we have is a whole complicated *optical machine*. But here's the trick we can think of the optical machine as a mathematical function that takes parmeters (components properties) and returns one scalar value (*objective fucntion value*). So basically we do have a mathematical-like function after all, that the *optimizer* can play with.


---
## User guide
#### 1. Install dependencies
> :warning: All instructions in this section are for ***Ubuntu systems***. <br> Note: instead of installing dependencies one by one you can run **```bash install.sh```** in the terminal in the folder where the file **```install.sh```** exists.
* Install C++ compiler **g++** to compile the source code ```sudo apt-get install g++```
* Install **make** to run all needed commands ```sudo apt install make```
* Install **gnuplot** for optical paths plot visualization``` sudo apt-get install -y gnuplot ```
* Install **gnuplot-x11** to enable the use of **wxt** for interactive gnuplot visualization ```sudo apt-get install gnuplot-x11```
* Install **doxygen** to generate documentation ```sudo apt-get install doxygen```

#### 2. Run the simulator
> :warning: This part is not implemented yet and won't run anything. Alternativaly you can run the available tests, which already show what the software is capable of. 
* **Compile** the source code ```make compile```
* **Test** the application ```make test```

#### 3. Clean
* **Remve** the executable and all run time generated files ```make clean```
