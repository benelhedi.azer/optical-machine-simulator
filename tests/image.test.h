#include <iostream>
#include <opticalMachine.h>
#include <objectiveFunction.h>

void imageTest()
{
    std::cout << "image test: " << std::endl;

    OpticalMachine *om = new OpticalMachine("tests/components4.xml", "tests/photons3.csv", "/Desktop", 30);

    om->run();
    om->visualize();
}