#include <iostream>
#include <opticalMachine.h>
#include <objectiveFunction.h>
#include <parameterName.enum.h>
#include <gradientDecentOptimizer.h>

void optimizationTest()
{
    std::cout << "optimization test: " << std::endl;

    // running simple simulation (once)
    OpticalMachine *om = new OpticalMachine("tests/componentsX1.xml", "tests/photonsX1.csv", "/Desktop", 100);

    om->run();
    om->visualize();
    om->savePicture();

    // running optimizer
    ObjectiveFunction *of = new ObjectiveFunction(om);
    of->setVariable("Detector", POSITIONX);

    double accuracy = 0.1;
    double h = 3;
    auto wrapper = [of](double value)
    {
        // std::cout << "x value: " << of->getOpticalMachine()->getComponent("Detector")->getPosition().getX() << std::endl;
        const double result = of->function(value);
        // std::cout << "result: " << result << std::endl;
        return result;
    };

    GradientDecentOptimizer optimizer1(12, 20, h, 0.1, wrapper, accuracy);
    LineSearchOptimizer optimizer2(12, 20, h, 0.1, wrapper);

    double result1 = optimizer1.run();
    double result2 = optimizer2.run();

    std::cout << "Gradient descent optimizer result: " << result1 << std::endl;
    std::cout << "Line search optimizer result: " << result2 << std::endl;
}