#pragma once
#include <opticalMachine.h>
#include <roundMirror.h>
#include <roundFilter.h>
#include <roundLens.h>
#include <roundDetector.h>
#include <photon.h>
#include <tools.h>

namespace Lens1
{
    Position position(3, 2, 2);
    Position normal(-1, 0, 0);
    double radius = 1.5;
    double focalLength = 15;
    RoundLens *component = new RoundLens("Lens 1", position, normal, radius, focalLength);
}

namespace Filter
{
    Position position(7, 2, 2);
    Position normal(1, 0, 0);
    double radius = 1;
    double minWL = 450;
    double maxWL = 600;
    RoundFilter *component = new RoundFilter("Filter", position, normal, radius, minWL, maxWL);
}

namespace Mirror1
{
    Position position(12, 2, 2);
    Position normal(-1, 1, 1);
    double radius = 1;
    RoundMirror *component = new RoundMirror("Mirror 1", position, normal, radius);
}

namespace Mirror2
{
    Position position(13.5, 5, 5);
    Position normal(0, -1, -1);
    double radius = 0.6;
    RoundMirror *component = new RoundMirror("Mirror 2", position, normal, radius);
}

namespace Mirror3
{
    Position position(14.5, 3, 3);
    Position normal(-1, 1, 2);
    double radius = 0.5;
    RoundMirror *component = new RoundMirror("Mirror 3", position, normal, radius);
}

namespace Mirror4
{
    Position position(12, 3.5, 7.5);
    Position normal(0, -1, -0.3);
    double radius = 1.5;
    RoundMirror *component = new RoundMirror("Mirror 4", position, normal, radius);
}

namespace Lens2
{
    Position position(10.5, 1, 10);
    Position normal(0, 0, -1);
    double radius = 1.5;
    double focalLength = 2.5;
    RoundLens *component = new RoundLens("Lens 2", position, normal, radius, focalLength);
}

namespace Detector
{
    Position position(9, -0.5, 12);
    Position normal(0, 0, -1);
    double radius = 0.8;
    RoundDetector *component = new RoundDetector("Detector", position, normal, radius);
}

namespace Photon1
{
    Position position(2, 2, 2);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_0");
}

namespace Photon2
{
    Position position(2, 3, 2);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_1");
}

namespace Photon3
{
    Position position(2, 1, 2);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_2");
}

namespace Photon4
{
    Position position(2, 2, 3);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_3");
}

namespace Photon5
{
    Position position(2, 2, 1);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_4");
}

namespace Photon6
{
    Position position(2, 2.5, 2.5);
    Position direction(1, 0, 0);
    double waveLength(400);
    Photon *photon = new Photon(position, direction, waveLength, "photon_5");
}

namespace Photon7
{
    Position position(5, 5, 5);
    Position direction(1, 0, 0);
    double waveLength(500);
    Photon *photon = new Photon(position, direction, waveLength, "photon_6");
}

void opticalMachineTest()
{
    std::cout << "\nStart optical machine test:\n";

    OpticalMachine om;

    om.addPhoton(Photon1::photon);
    om.addPhoton(Photon2::photon);
    om.addPhoton(Photon3::photon);
    om.addPhoton(Photon4::photon);
    om.addPhoton(Photon5::photon);
    om.addPhoton(Photon6::photon);

    om.addComponent(Lens1::component);
    om.addComponent(Filter::component);
    om.addComponent(Mirror1::component);
    om.addComponent(Mirror2::component);
    om.addComponent(Mirror3::component);
    om.addComponent(Mirror4::component);
    om.addComponent(Lens2::component);
    om.addComponent(Detector::component);

    om.run();

    om.visualize();

    // Mirror::component->saveComponentToFile();

    std::cout << "\nEnd optical machine test:\n";
}
