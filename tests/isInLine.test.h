#pragma once
#include <iostream>
#include <position.h>
#include <photon.h>
#include <color.h>

void isInLineTest(){
    std::cout << "\n_________________\n\n";
    std::cout << "intersection test\n";
    Position
        point1 (2,10,6),
        point2 (2,3,6),
        point3 (2,10,8),
        point4 (2,3,3),
        point5 (1,2,3),
        point6 (4,8,12), 
        php(0,0,0), 
        phd(1,2,3);
    Photon ph(php,phd,500, "photon_0");
    std::cout << "test 1 [o: failure, 1: success]: " << (false == ph.isInLine(point1)) << "\n";
    std::cout << "test 2 [o: failure, 1: success]: " << (false == ph.isInLine(point2)) << "\n";
    std::cout << "test 3 [o: failure, 1: success]: " << (false == ph.isInLine(point3)) << "\n";
    std::cout << "test 4 [o: failure, 1: success]: " << (false == ph.isInLine(point4)) << "\n";
    std::cout << "test 5 [o: failure, 1: success]: " << (true == ph.isInLine(point5)) << "\n";
    std::cout << "test 6 [o: failure, 1: success]: " << (true == ph.isInLine(point6)) << "\n";
    std::cout << "\n_________________\n\n";
}