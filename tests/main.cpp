#include <iostream>
#include <photon.h>
#include <mirror_filter.test.h>
#include <opticalComponent.h>
#include <threeD.test.h>
#include <normalVector.test.h>
#include <isInLine.test.h>
#include <optimizer.test.h>
#include <openMP.test.h>
#include <optimization.test.h>
#include <image.test.h>

// for the following two paths only one should be included
//#include <opticalMachine.test.h>
#include <opticalMachineWithCSV.test.h>

int main(int args, char *argv[])
{
  std::cout << "Photon Simulator tests\n\n";

  threeDTest();
  normalVectorTest();
  std::cout << "\n";
  roundMirrorTest();
  std::cout << "\n";
  roundFilterTest();
  isInLineTest();
  // opticalMachineTest();
  opticalMachineTestWithCSV();
  optimizerTest();
  openMPTest();
  //imageTest();
  optimizationTest();

  return 0;
}