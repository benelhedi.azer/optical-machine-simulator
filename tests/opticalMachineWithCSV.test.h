#pragma once
#include <opticalMachine.h>

void opticalMachineTestWithCSV()
{
    std::cout << "\nStart optical machine test:\n";

    OpticalMachine om("tests/components.xml", "tests/photons2.csv", "", 500);

    om.run();
    //om.visualize();

    std::cout << "\nEnd optical machine test:\n";
}
