#pragma once
#include <opticalMachine.h>
#include <ctime>
#include <iomanip>
#include <chrono>

void openMPTest()
{
    std::cout << "\nStart oopenMP test:\n";

    OpticalMachine om("tests/components.xml", "tests/photons2.csv", "", 500);

    auto realStart = std::chrono::high_resolution_clock::now();
    std::clock_t start = std::clock();
    om.run();
    auto realEnd = std::chrono::high_resolution_clock::now();
    std::clock_t end = std::clock();

    std::cout << std::fixed << std::setprecision(2) << "Simulation time (CPU time): "
              << 1000.0 * (end - start) / CLOCKS_PER_SEC << " ms\n"
              << "Simulation time (real time) "
              << std::chrono::duration<double, std::milli>(realEnd - realStart).count()
              << " ms\n";

    std::cout
        << "\nEnd openMP test:\n";
}
