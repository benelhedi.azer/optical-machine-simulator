#pragma once
#include <position.h>

void normalVectorTest(){
    std::cout << "Normal vector test:\n";
    Position p(1,2,3);
    std::cout << p.normal() << " normal to " << p << "\n";
}
