#pragma once
#include <iostream>
#include <vector>
#include <photon.h>
#include <roundMirror.h>
#include <roundFilter.h>
#include <random>

void roundMirrorTest()
{
    std::cout << "RoundMirror test\n";
    Position p(0, 1, 0), d(1, 0, 0);
    double wavelength = 500;
    Photon photon(p, d, wavelength, "photon_0");
    std::cout << "old direction = " << photon.getDirection() << "\n";
    Photon* pointer = &photon;

    Position pm(10, 0, 0), n(-1, 1, 0);
    double r = 11;
    RoundMirror mirror("mirror", pm, n, r);

    //test normalize() in ThreeD
    ThreeD N = n.normalize();
    std::cout << "normalized normal vector = " << N << "\n";
    
    mirror.computePhotonDirection(pointer);
    std::cout << "new direction = " << pointer->getDirection() << "\n";
    std::cout << "expected = {0, 1, 0}" << "\n";
}

void roundFilterTest()
{
    std::cout << "RoundFilter test\n";
    Position p(0, 1, 0), d(1, 0, 0);

    //random generator [380, 750]
    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_int_distribution<int> distr(380, 750);
    double wavelength = distr(eng);
    std::cout << "wavelength = " << wavelength << "nm\n";

    Photon photon(p, d, wavelength, "photon_0");
    std::cout << "old direction = " << photon.getDirection() << "\n";
    Photon* pointer = &photon;

    Position pf(10, 0, 0), n(-1, 0, 0);
    double r = 11;
    double minWaveLength = 500;
    double maxWaveLength = 600;
    RoundFilter filter("filter", pf, n, r, minWaveLength, maxWaveLength);
    
    filter.computePhotonDirection(pointer);
    std::cout << "new direction = " << pointer->getDirection() << "\n";
}