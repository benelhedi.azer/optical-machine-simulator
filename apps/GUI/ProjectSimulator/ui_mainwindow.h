/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *Start;
    QLabel *Welcome;
    QPushButton *Area;
    QPushButton *Browse;
    QLabel *File;
    QPushButton *Next;
    QPushButton *Browse2;
    QLabel *File2;
    QLabel *Logo;
    QLabel *Text;
    QLabel *Text2;
    QLabel *Header;
    QLabel *Logo2;
    QLabel *Header2;
    QPushButton *Next2;
    QLabel *Text3;
    QLabel *Header3;
    QPushButton *Restart;
    QPushButton *Optimize;
    QLineEdit *Input;
    QPushButton *SetValue;
    QLabel *Header4;
    QPushButton *SetDirectory;
    QPushButton *Next3;
    QLabel *Directory;
    QLabel *Text4;
    QLabel *Image;
    QPushButton *Test;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(451, 402);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        Start = new QPushButton(centralwidget);
        Start->setObjectName(QString::fromUtf8("Start"));
        Start->setGeometry(QRect(0, 0, 65, 32));
        Welcome = new QLabel(centralwidget);
        Welcome->setObjectName(QString::fromUtf8("Welcome"));
        Welcome->setGeometry(QRect(90, 10, 58, 16));
        Area = new QPushButton(centralwidget);
        Area->setObjectName(QString::fromUtf8("Area"));
        Area->setGeometry(QRect(0, 120, 65, 32));
        Browse = new QPushButton(centralwidget);
        Browse->setObjectName(QString::fromUtf8("Browse"));
        Browse->setGeometry(QRect(0, 90, 65, 32));
        File = new QLabel(centralwidget);
        File->setObjectName(QString::fromUtf8("File"));
        File->setGeometry(QRect(180, 70, 58, 16));
        Next = new QPushButton(centralwidget);
        Next->setObjectName(QString::fromUtf8("Next"));
        Next->setGeometry(QRect(0, 60, 65, 32));
        Browse2 = new QPushButton(centralwidget);
        Browse2->setObjectName(QString::fromUtf8("Browse2"));
        Browse2->setGeometry(QRect(0, 30, 65, 32));
        File2 = new QLabel(centralwidget);
        File2->setObjectName(QString::fromUtf8("File2"));
        File2->setGeometry(QRect(180, 40, 58, 16));
        Logo = new QLabel(centralwidget);
        Logo->setObjectName(QString::fromUtf8("Logo"));
        Logo->setGeometry(QRect(180, 10, 58, 16));
        Text = new QLabel(centralwidget);
        Text->setObjectName(QString::fromUtf8("Text"));
        Text->setGeometry(QRect(90, 100, 58, 16));
        Text2 = new QLabel(centralwidget);
        Text2->setObjectName(QString::fromUtf8("Text2"));
        Text2->setGeometry(QRect(90, 130, 58, 16));
        Header = new QLabel(centralwidget);
        Header->setObjectName(QString::fromUtf8("Header"));
        Header->setGeometry(QRect(90, 40, 58, 16));
        Logo2 = new QLabel(centralwidget);
        Logo2->setObjectName(QString::fromUtf8("Logo2"));
        Logo2->setGeometry(QRect(180, 100, 58, 16));
        Header2 = new QLabel(centralwidget);
        Header2->setObjectName(QString::fromUtf8("Header2"));
        Header2->setGeometry(QRect(90, 70, 58, 16));
        Next2 = new QPushButton(centralwidget);
        Next2->setObjectName(QString::fromUtf8("Next2"));
        Next2->setGeometry(QRect(0, 150, 65, 32));
        Text3 = new QLabel(centralwidget);
        Text3->setObjectName(QString::fromUtf8("Text3"));
        Text3->setGeometry(QRect(180, 130, 58, 16));
        Header3 = new QLabel(centralwidget);
        Header3->setObjectName(QString::fromUtf8("Header3"));
        Header3->setGeometry(QRect(180, 130, 58, 16));
        Restart = new QPushButton(centralwidget);
        Restart->setObjectName(QString::fromUtf8("Restart"));
        Restart->setGeometry(QRect(0, 180, 65, 32));
        Optimize = new QPushButton(centralwidget);
        Optimize->setObjectName(QString::fromUtf8("Optimize"));
        Optimize->setGeometry(QRect(0, 210, 65, 32));
        Input = new QLineEdit(centralwidget);
        Input->setObjectName(QString::fromUtf8("Input"));
        Input->setGeometry(QRect(90, 210, 113, 21));
        SetValue = new QPushButton(centralwidget);
        SetValue->setObjectName(QString::fromUtf8("SetValue"));
        SetValue->setGeometry(QRect(0, 240, 65, 32));
        Header4 = new QLabel(centralwidget);
        Header4->setObjectName(QString::fromUtf8("Header4"));
        Header4->setGeometry(QRect(90, 250, 58, 16));
        SetDirectory = new QPushButton(centralwidget);
        SetDirectory->setObjectName(QString::fromUtf8("SetDirectory"));
        SetDirectory->setGeometry(QRect(0, 270, 65, 32));
        Next3 = new QPushButton(centralwidget);
        Next3->setObjectName(QString::fromUtf8("Next3"));
        Next3->setGeometry(QRect(0, 300, 65, 32));
        Directory = new QLabel(centralwidget);
        Directory->setObjectName(QString::fromUtf8("Directory"));
        Directory->setGeometry(QRect(180, 250, 58, 16));
        Text4 = new QLabel(centralwidget);
        Text4->setObjectName(QString::fromUtf8("Text4"));
        Text4->setGeometry(QRect(260, 250, 58, 16));
        Image = new QLabel(centralwidget);
        Image->setObjectName(QString::fromUtf8("Image"));
        Image->setGeometry(QRect(90, 280, 58, 16));
        Test = new QPushButton(centralwidget);
        Test->setObjectName(QString::fromUtf8("Test"));
        Test->setGeometry(QRect(90, 280, 231, 51));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 451, 24));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Test->setText(QCoreApplication::translate("MainWindow", "test button for displaying image", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
