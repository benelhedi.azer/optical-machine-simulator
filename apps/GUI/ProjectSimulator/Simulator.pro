QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

QMAKE_LFLAGS += -fopenmp

# no optimzation
QMAKE_CFLAGS -= -O2

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    fonts.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../build/lib/release/ -loptiks
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../build/lib/debug/ -loptiks
else:unix: LIBS += -L$$PWD/../../../build/lib/ -loptiks

INCLUDEPATH += $$PWD/../../../build/include/optiks
DEPENDPATH += $$PWD/../../../build/include/optiks

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../build/lib/release/liboptiks.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../build/lib/debug/liboptiks.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../build/lib/release/optiks.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../build/lib/debug/optiks.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../build/lib/liboptiks.a
