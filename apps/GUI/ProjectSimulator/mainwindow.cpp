#include "main.cpp"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <types.h>
#include <opticalMachine.h>
#include <objectiveFunction.h>
#include <lineSearchOptimizer.h>
#include <QtWidgets>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //set the background
    QPalette palette;
    palette.setColor(QPalette::Window, QColor(255,255,255,220));
    this->setAutoFillBackground(true);
    this->setPalette(palette);
    this->show();

    //RWTH logo
    QPixmap pic("/home/azer/Documents/projects/simulator/apps/GUI/ProjectSimulator/RWTH_logo.png");
    ui->Logo->setPixmap(pic.scaled(size()*0.42, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->Logo->setFixedSize(200, 50);
    ui->Logo->move(920, 570);

    //folder icon
    QPixmap icon("/home/azer/Documents/projects/simulator/apps/GUI/ProjectSimulator/icon.png");
    ui->Logo2->setPixmap(icon.scaled(size()*0.09, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->Logo2->setFixedSize(80, 80);
    ui->Logo2->move(559, 190);

    ui->Welcome->setText("Welcome to the simulator.");
    QFont font_welcome("Poppins", 11, QFont::Medium);
    ui->Welcome->setFont(font_welcome);
    ui->Welcome->setStyleSheet("color: #6A6E83");
    ui->Welcome->setFixedSize(1152, 40);
    ui->Welcome->move(0, 110);
    ui->Welcome->setAlignment(Qt::AlignCenter);

    ui->Header->setText("Define the components of the optical machine.");
    QFont font_header("Poppins", 11, QFont::Medium);
    ui->Header->setFont(font_header);
    ui->Header->setStyleSheet("color: #6A6E83");
    ui->Header->setFixedSize(1152, 40);
    ui->Header->move(0, 110);
    ui->Header->setAlignment(Qt::AlignCenter);
    ui->Header->setVisible(false);

    ui->Header2->setText("Define the total number of photons.");
    ui->Header2->setFont(font_header);
    ui->Header2->setStyleSheet("color: #6A6E83");
    ui->Header2->setFixedSize(1152, 40);
    ui->Header2->move(0, 110);
    ui->Header2->setAlignment(Qt::AlignCenter);
    ui->Header2->setVisible(false);

    ui->Header3->setText("Your simulation is complete.");
    ui->Header3->setFont(font_header);
    ui->Header3->setStyleSheet("color: #6A6E83");
    ui->Header3->setFixedSize(1152, 40);
    ui->Header3->move(0, 80);
    ui->Header3->setAlignment(Qt::AlignCenter);
    ui->Header3->setVisible(false);

    ui->Header4->setText("Define the path for saving files.");
    ui->Header4->setFont(font_header);
    ui->Header4->setStyleSheet("color: #6A6E83");
    ui->Header4->setFixedSize(1152, 40);
    ui->Header4->move(0, 110);
    ui->Header4->setAlignment(Qt::AlignCenter);
    ui->Header4->setVisible(false);

    ui->Area->setStyleSheet("background-color: #EFF4FF; border-radius: 15px");
    ui->Area->setFixedSize(290, 210);
    ui->Area->move(431, 180);

    ui->Text->setText("Upload your source file\nin CSV format.");
    QFont font_text("Poppins", 7);
    ui->Text->setFont(font_text);
    ui->Text->setStyleSheet("color: #B9BDCD");
    ui->Text->setFixedSize(1152, 60);
    ui->Text->move(0, 250);
    ui->Text->setAlignment(Qt::AlignCenter);

    ui->Text2->setText("Upload your source file\nin XML format.");
    ui->Text2->setFont(font_text);
    ui->Text2->setStyleSheet("color: #B9BDCD");
    ui->Text2->setFixedSize(1152, 60);
    ui->Text2->move(0, 250);
    ui->Text2->setAlignment(Qt::AlignCenter);
    ui->Text2->setVisible(false);

    ui->Text3->setText("How many photons should be simulated\nat the start of the optical path?");
    ui->Text3->setFont(font_text);
    ui->Text3->setStyleSheet("color: #B9BDCD");
    ui->Text3->setFixedSize(1152, 60);
    ui->Text3->move(0, 210);
    ui->Text3->setAlignment(Qt::AlignCenter);
    ui->Text3->setVisible(false);

    ui->Text4->setText("Where do you want to\nsave new files?");
    ui->Text4->setFont(font_text);
    ui->Text4->setStyleSheet("color: #B9BDCD");
    ui->Text4->setFixedSize(1152, 60);
    ui->Text4->move(0, 250);
    ui->Text4->setAlignment(Qt::AlignCenter);
    ui->Text4->setVisible(false);

    //input box: only a number between the defined range is allowed
    ui->Input->setValidator(new QIntValidator(0, 100000000, this));
    ui->Input->setFixedSize(120, 25);
    ui->Input->move(516, 280);
    ui->Input->setAlignment(Qt::AlignCenter);
    ui->Input->setVisible(false);

    ui->Browse->setText(" Browse Files");
    QFont font_browse("Poppins", 7, QFont::DemiBold);
    ui->Browse->setFont(font_browse);
    ui->Browse->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 10px");
    ui->Browse->setFixedSize(105, 32);
    ui->Browse->move(525, 330);

    ui->Browse2->setText(" Browse Files");
    ui->Browse2->setFont(font_browse);
    ui->Browse2->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 10px");
    ui->Browse2->setFixedSize(105, 32);
    ui->Browse2->move(525, 330);
    ui->Browse2->setVisible(false);

    ui->SetDirectory->setText(" Set Directory");
    ui->SetDirectory->setFont(font_browse);
    ui->SetDirectory->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 10px");
    ui->SetDirectory->setFixedSize(110, 32);
    ui->SetDirectory->move(521, 330);
    ui->SetDirectory->setVisible(false);

    ui->SetValue->setText(" Set Value");
    ui->SetValue->setFont(font_browse);
    ui->SetValue->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 10px");
    ui->SetValue->setFixedSize(90, 32);
    ui->SetValue->move(531, 340);
    ui->SetValue->setVisible(false);

    ui->Next->setText(" Next");
    QFont font_next("Poppins", 7, QFont::DemiBold);
    ui->Next->setFont(font_next);
    ui->Next->setStyleSheet("background-color: #EFF4FF; color: #B9BDCD; border-radius: 20px");
    ui->Next->setFixedSize(76, 40);
    ui->Next->move(538, 450);
    ui->Next->setEnabled(false);

    ui->Next2->setText(" Next");
    ui->Next2->setFont(font_next);
    ui->Next2->setStyleSheet("background-color: #EFF4FF; color: #B9BDCD; border-radius: 20px");
    ui->Next2->setFixedSize(76, 40);
    ui->Next2->move(538, 450);
    ui->Next2->setVisible(false);

    ui->Next3->setText(" Next");
    ui->Next3->setFont(font_next);
    ui->Next3->setStyleSheet("background-color: #EFF4FF; color: #B9BDCD; border-radius: 20px");
    ui->Next3->setFixedSize(76, 40);
    ui->Next3->move(538, 450);
    ui->Next3->setVisible(false);

    ui->Start->setText(" Start Simulation");
    //QFont font_start("Poppins", 12, QFont::DemiBold);
    ui->Start->setFont(font_next);
    ui->Start->setStyleSheet("background-color: #EFF4FF; color: #B9BDCD; border-radius: 20px");
    ui->Start->setFixedSize(156, 40);
    ui->Start->move(498, 440);
    ui->Start->setVisible(false);

    ui->Optimize->setText(" Optimize");
    ui->Optimize->setFont(font_next);
    ui->Optimize->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 20px");
    ui->Optimize->setFixedSize(150, 40);
    ui->Optimize->move(501, 420);
    ui->Optimize->setVisible(false);

    ui->Restart->setText(" Restart");
    ui->Restart->setFont(font_next);
    ui->Restart->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 20px");
    ui->Restart->setFixedSize(150, 40);
    ui->Restart->move(501, 470);
    ui->Restart->setVisible(false);

    //hide test button
    ui->Test->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Browse_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Choose CSV-file"), "/Users", tr("CSV files (*.csv)"));

    //save and print path of selected file
    path_csv = file;
    qDebug() << "path of CSV-file: " << path_csv;

    //display path of selected file
    ui->File->setText(file);
    QFont font_filename("Poppins", 7);
    ui->File->setFont(font_filename);
    ui->File->setStyleSheet("color: #6A6E83");
    ui->File->setFixedSize(1152, 20);
    ui->File->move(0, 400);
    ui->File->setAlignment(Qt::AlignCenter);

    //enable "Next" button
    ui->Next->setEnabled(true);
    ui->Next->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 20px");
}

void MainWindow::on_Browse2_clicked()
{
    QString file2 = QFileDialog::getOpenFileName(this, tr("Choose XML-file"), "/Users", tr("XML files (*.xml)"));

    //save and print path of selected file
    path_xml = file2;
    qDebug() << "path of XML-file: " << path_xml;

    //display path of selected file
    ui->File2->setText(file2);
    QFont font_filename2("Poppins", 7);
    ui->File2->setFont(font_filename2);
    ui->File2->setStyleSheet("color: #6A6E83");
    ui->File2->setFixedSize(1152, 20);
    ui->File2->move(0, 400);
    ui->File2->setAlignment(Qt::AlignCenter);

    //enable "Next" button
    ui->Next2->setEnabled(true);
    ui->Next2->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 20px");
}

void MainWindow::on_Next_clicked()
{
    //hide old buttons
    ui->Welcome->setVisible(false);
    ui->Text->setVisible(false);
    ui->Browse->setVisible(false);
    ui->File->setVisible(false);
    ui->Next->setVisible(false);

    //show new buttons
    ui->Header->setVisible(true);
    ui->Text2->setVisible(true);
    ui->Browse2->setVisible(true);
    ui->Next2->setVisible(true);

    ui->Next2->setEnabled(false);
}

void MainWindow::on_Next2_clicked()
{
    //hide old buttons
    ui->Header->setVisible(false);
    ui->Text2->setVisible(false);
    ui->Browse2->setVisible(false);
    ui->File2->setVisible(false);
    ui->Next2->setVisible(false);

    //show new buttons
    ui->Header4->setVisible(true);
    ui->Text4->setVisible(true);
    ui->SetDirectory->setVisible(true);
    ui->Next3->setVisible(true);

    ui->Next3->setEnabled(false);
}

void MainWindow::on_Next3_clicked()
{
    //hide old buttons
    ui->Header4->setVisible(false);
    ui->Logo2->setVisible(false);
    ui->Text4->setVisible(false);
    ui->SetDirectory->setVisible(false);
    ui->Directory->setVisible(false);
    ui->Next3->setVisible(false);

    //show new buttons
    ui->Header2->setVisible(true);
    ui->Text3->setVisible(true);
    ui->Input->setVisible(true);
    ui->SetValue->setVisible(true);
    ui->Start->setVisible(true);

    ui->Start->setEnabled(false);

    ui->Area->setFixedSize(320, 225);
    ui->Area->move(416, 180);
}

void MainWindow::on_SetDirectory_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/Users", QFileDialog::ShowDirsOnly);

    //save and print path of selected file
    path_directory = directory;
    qDebug() << "path of directory: " << path_directory;

    //display path of selected file
    ui->Directory->setText(directory);
    QFont font_directory("Poppins", 7);
    ui->Directory->setFont(font_directory);
    ui->Directory->setStyleSheet("color: #6A6E83");
    ui->Directory->setFixedSize(1152, 20);
    ui->Directory->move(0, 400);
    ui->Directory->setAlignment(Qt::AlignCenter);

    //enable "Next" button
    ui->Next3->setEnabled(true);
    ui->Next3->setStyleSheet("background-color: #5A81FA; color: #FFFFFF; border-radius: 20px");
}

void MainWindow::on_SetValue_clicked()
{
    //save and print input number
    number_photons = ui->Input->text().toInt();
    qDebug() << "total number of photons: " << number_photons;

    //enable "Start Simulation" button
    ui->Start->setEnabled(true);
    ui->Start->setStyleSheet("background-color: #2C3D8F; color: #FFFFFF; border-radius: 20px");
}

void MainWindow::on_Start_clicked()
{
   startSimulation();
}

void MainWindow::startSimulation()
{
    // start optical machine
    om = new OpticalMachine(path_xml.toStdString(), path_csv.toStdString(), "", number_photons);

    // run simulation of optical machine
    om->run();

    // generate image (with black backround)
    QImage finished_image(256, 256, QImage::Format_RGB888);
    finished_image.fill(QColor(Qt::black).rgb());

    Picture pic = om->getPicture();
    int size = pic.size();

    // finding the maximum value of all pixels (needed for normalization of pixel values)
    double max = 1;
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            if (pic[i][j] > max){
                max = pic[i][j];
            };
        };
    };
    std::cout << "Max: " <<max << std::endl;

    // normalize and save pixel values to finished_image
    int color;
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){

            // normalize pixel
            color = std::round((pic[i][j]/max)*255);

            // save pixel to image
            finished_image.setPixel(i, j, qRgb(color, color, color));

            std::cout << i << "," << j <<": "  << color << std::endl;

        };
    };

    QString fullPath = path_directory + QString::fromStdString("/result.png");
    std::cout << fullPath.toStdString() << std::endl;

    bool saveResultState = finished_image.save(fullPath);
    std::cout << (saveResultState ? "good" : "bad") << std::endl;

    ui->Image->setPixmap(QPixmap::fromImage(finished_image));
    ui->Image->setFixedSize(300, 300);
    ui->Image->move(445, 90);

    //hide old buttons
    ui->Header2->setVisible(false);
    ui->Area->setVisible(false);
    ui->Text3->setVisible(false);
    ui->Input->setVisible(false);
    ui->SetValue->setVisible(false);
    ui->Start->setVisible(false);

    //show new buttons
    //ui->Header3->setVisible(true);
    ui->Optimize->setVisible(true);
    ui->Restart->setVisible(true);
}

void MainWindow::on_Optimize_clicked()
{
    ObjectiveFunction *of = new ObjectiveFunction(om);
    of->setVariable("Detector", POSITIONX);

    // double accuracy = 2;
    auto wrapper = [of](double value)
    {
        std::cout << "x value: " << of->getOpticalMachine()->getComponent("Detector")->getPosition().getX() << std::endl;
        return of->function(value);
    };
    om->run();
    om->visualize();

    LineSearchOptimizer optimizer(11, 50, 2, 0.1, wrapper);
    double result = optimizer.run();

    std::cout << "optimization result [GUI call]: " << result << std::endl;
}

void MainWindow::on_Restart_clicked()
{
    //this->close();

    //open a new GUI window
    MainWindow *new_window = new MainWindow(this);
    new_window->setFixedSize(1152, 648);
    new_window->setWindowTitle("Optical Path Simulator");
    new_window->show();
}

void MainWindow::on_Test_clicked()
{
    //test
    QImage finished_image(256, 256, QImage::Format_RGB888);
    finished_image.fill(QColor(Qt::white).rgb());

    for (int x = 0; x < 10; ++x) {
        for (int y = 0; y < 10; ++y) {
            finished_image.setPixel(x, y, qRgb(200, 200, 200));
        }
    }

    ui->Image->setPixmap(QPixmap::fromImage(finished_image));
    ui->Image->setFixedSize(300, 300);
    ui->Image->move(500, 200);
}
