### Qt GUI Application
This folder contains a Qt application source code that uses the optiks Library.

The Qt application is independant of the library and has its own run and build infrastructure that you can control by Qt-creator

The Qt source is located in the same repository as the optiks library to make all code easily accessible by developers.

To work on the GUI application please open the `ProjectSimulator_v2` folder as a project in Qt-creator.

