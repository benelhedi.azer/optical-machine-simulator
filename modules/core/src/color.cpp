#include <color.h>

bool Color::isValid(double x, double y, double z)
{
  if (x < 0 || y < 0 || z < 0)
    return false;

  if (x > 255 || y > 255 || z > 255)
    return false;

  return true;
}

Color::Color(double x, double y, double z) : ThreeD(x, y, z)
{
  if (!this->isValid(x, y, z))
    throw std::invalid_argument("invalid arguments to create ThreeD instance");
}
