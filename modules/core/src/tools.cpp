#include <position.h>
#include <formatter.h>
#include <types.h>
#include <algorithm>
#include <string>

namespace tools
{
    Position linePlaneIntersection(Position planePosition, Position linePosition, Position normal, Position lineDirection)
    {
        double step = ((planePosition - linePosition) * normal) / (lineDirection * normal);
        ThreeD intersection = linePosition + lineDirection * step;
        return intersection.convert<Position>();
    }

    Formatter format(int w, std::string s)
    {
        return Formatter::set(w, s.substr(0, 1), s.substr(1, 1), s.substr(2, 1));
    }

    Formatter format(int w, std::string ow, std::string s, std::string cw)
    {
        return Formatter::set(w, ow, s, cw);
    }

    StringVector split(std::string str, std::string seperator)
    {
        StringVector result;
        while (str.find(seperator) != -1)
        {
            int position = str.find(seperator);
            result.push_back(str.substr(0, position));
            str = str.substr(position + 1);
        }
        result.push_back(str);
        return result;
    }

    double stringToDouble(std::string str)
    {
        const std::string testString = "1.7";
        const double testDouble = 1.7;

        const bool shouldConvertToCommaSeperator = (std::stod(testString) != testDouble);

        if (shouldConvertToCommaSeperator)
        {
            std::replace(str.begin(), str.end(), '.', ',');
        }

        return std::stod(str);
    }

}