#include <threeD.h>
#include <stdexcept>
#include <math.h>
#include <position.h>
#include <angle.h>
#include <color.h>
#include <iomanip>
#include <iostream>
#include <formatter.h>

ThreeD::ThreeD() : _x(0), _y(0), _z(0) {}

ThreeD::ThreeD(double x, double y, double z) : _x(x), _y(y), _z(z)
{
  if (!this->isValid(x, y, z))
    throw std::invalid_argument("invalid arguments to create ThreeD instance");
}

double ThreeD::getX() const
{
  return _x;
}

double ThreeD::getY() const
{
  return _y;
}

double ThreeD::getZ() const
{
  return _z;
}

void ThreeD::setX(double x)
{
  _x = x;
}

void ThreeD::setY(double y)
{
  _y = y;
}

void ThreeD::setZ(double z)
{
  _z = z;
}
ThreeD ThreeD::operator+(ThreeD a)
{
  ThreeD r(_x + a.getX(), _y + a.getY(), _z + a.getZ());
  return r;
}

ThreeD ThreeD::operator-(ThreeD a)
{
  ThreeD r(_x - a.getX(), _y - a.getY(), _z - a.getZ());
  return r;
}

ThreeD ThreeD::operator=(ThreeD a)
{
  _x = a.getX();
  _y = a.getY();
  _z = a.getZ();
  return *this;
}

ThreeD ThreeD::operator*(double a)
{
  ThreeD r(_x * a, _y * a, _z * a);
  return r;
}

double ThreeD::operator*(ThreeD a)
{
  return _x * a.getX() + _y * a.getY() + _z * a.getZ();
}

bool ThreeD::isValid(double x, double y, double z)
{
  return true;
}

std::ostream &operator<<(std::ostream &os, const ThreeD &a)
{
  const int width = Formatter::width;
  const std::string openWrapper = Formatter::openWrapper;
  const std::string seperator = Formatter::seperator;
  const std::string closeWrapper = Formatter::closeWrapper;

  os << openWrapper << std::left << std::setw(width) << std::setprecision(15) << a.getX() << seperator;
  os << std::left << std::setw(width) << std::setprecision(15) << a.getY() << seperator;
  os << std::left << std::setw(width) << std::setprecision(15) << a.getZ() << closeWrapper;
  return os;
}

ThreeD ThreeD::normal()
{
  ThreeD r;

  if (_z != 0)
  {
    r = ThreeD(1, 1, 0);
    r.setZ(-(_x * r.getX() + _y * r.getY()) / _z);
  }

  else if (_y != 0)
  {
    r = ThreeD(1, 0, 1);
    r.setY(-(_x * r.getX() + _z * r.getZ()) / _y);
  }

  else if (_x != 0)
  {
    r = ThreeD(0, 1, 1);
    r.setX(-(_y * r.getY() + _z * r.getZ()) / _x);
  }

  else
  {
    // Error handling should be implemented here
    std::cout << "Can't find normal vector to" << *this << "\n";
  }

  return r;
}

double ThreeD::norm()
{
  return sqrt(_x * _x + _y * _y + _z * _z);
}

ThreeD ThreeD::normalize()
{
  double magnitude = norm();
  double rx = _x / magnitude;
  double ry = _y / magnitude;
  double rz = _z / magnitude;
  ThreeD r = ThreeD(rx, ry, rz);
  return r;
}

ThreeD ThreeD::operator-(double a)
{
  ThreeD r;
  r.setX(_x - a);
  r.setY(_y - a);
  r.setZ(_z - a);
  return r;
}

ThreeD ThreeD::operator+(double a)
{
  ThreeD r;
  r.setX(_x + a);
  r.setY(_y + a);
  r.setZ(_z + a);
  return r;
}

ThreeD ThreeD::operator-()
{
  ThreeD r(-_x, -_y, -_z);
  return r;
}

ThreeD operator/(double n, ThreeD a)
{
  ThreeD r(1 / a.getX(), 1 / a.getY(), 1 / a.getZ());
  return r;
}

ThreeD ThreeD::elementWiseProduct(ThreeD a)
{
  ThreeD r(a.getX() * _x, a.getY() * _y, a.getZ() * _z);
  return r;
}

ThreeD ThreeD::crossProdcut(ThreeD b)
{
  return ThreeD(_y * b.getZ() - b.getY() * _z, _z * b.getX() - b.getZ() * _x, _x * b.getY() - b.getX() * _y) ;
}

bool ThreeD::cmp(ThreeD a)
{
  return _x == a.getX() && _y == a.getY() && _z == a.getZ();
}

template <class T>
T ThreeD::convert()
{
  T r(_x, _y, _z);
  return r;
}

template Position ThreeD::convert<Position>();
template Angle ThreeD::convert<Angle>();
template Color ThreeD::convert<Color>();