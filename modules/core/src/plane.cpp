#include <plane.h>
#include <math.h>

Plane::Plane(ThreeD normal)
{
    v1 = normal.normal().normalize();
    v2 = v1.crossProdcut(normal).normalize();
}

ThreeD Plane::getPoint(double radius, double angle)
{
    const double PI = 3.14159265;
    double radAngle = angle * PI / 180.0;
    return (v1 * radius) * cos(radAngle) + (v2 * radius) * sin(radAngle);
}