#include <formatter.h>

// Default constructor
Formatter::Formatter() {}

// Public member method(s)
Formatter Formatter::set(int w, std::string ow, std::string s, std::string cw)
{
    width = w;
    openWrapper = ow;
    seperator = s;
    closeWrapper = cw;
    return Formatter();
}

// Overloaded friend method(s)
std::ostream &operator<<(std::ostream &os, const Formatter &a)
{
    return os;
}

// Initializing static member variables
int Formatter::width = 0;
std::string Formatter::openWrapper = "{";
std::string Formatter::seperator = ",";
std::string Formatter::closeWrapper = "}";