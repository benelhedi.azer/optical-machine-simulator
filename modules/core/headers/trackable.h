#pragma once
#include <vector>
#include <iostream>

template <typename T>
class Trackable
{
private:
    std::string _id;
    std::vector<T> _log;

public:
    Trackable(std::string id);

    std::string getId()
    {
        return _id;
    }

    /*!
    Saves logged data from the vector to a file.
    */
    void virtual printLogToFile();

protected:
    /*!
    Adds one item to the log
    @param value T
    */
    void virtual _addToLog(T);
};
