#pragma once
#include <threeD.h>

class Plane
{
public:
    ThreeD v1, v2;

    // Constructor(s)
    Plane(ThreeD normal);

    // Public member method(s)

    /*!
    @param radius double in mm
    @param angle double in degree
    @return a point in this plan of type ThreeD with a position relative to the origin of the XYZ space where all component sit
    */
    ThreeD getPoint(double radius, double angle);
};