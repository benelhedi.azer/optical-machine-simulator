#pragma once
#include <roundOpticalComponent.h>
#include <iostream>
#include <string>

class RoundLens : public RoundOpticalComponent
{
double _fl; //focal length
public:

  /*!
  @param name std::string
  @param position Postion
  @param normal Postion
  @param radius double
  @param focalLength double
  */
  RoundLens(std::string name, Position p, Position n, double r, double fl);

  /*!
  Computes the photon direction that it should have after touching the surface of the component.
  @param photon Photon*
  */
  void computePhotonDirection(Photon* photon);
  
  /*!
  Returns focal length of the lens
  @return double
  */
  double getFocalPoint() const;
};