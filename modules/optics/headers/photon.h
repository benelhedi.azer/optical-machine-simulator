#pragma once
#include <position.h>
#include <color.h>
#include <opticalComponent.h>
#include <trackable.h>
#include <iostream>

class OpticalComponent;

class Photon : public Trackable<Position>
{
  Position _position;
  Position _direction;
  double _waveLength;

public:
  // Constructor
  /*!
  @param postion: Position
  @param direction: Positon
  @param id: std::string
  */
  Photon(Position p, Position d, double wl, std::string id);

  // Getters
  Position getPosition() const;
  Position getDirection() const;
  double getWaveLength() const;

  // Setters
  void setPosition(Position);
  void setDirection(Position);
  void setWaveLength(double);

  /*!
  @param p: Point
  @return bool
  checks whether a point belongs to the line of direction of the photon
  */
  bool isInLine(Position p);
};