#pragma once
#include <iostream>
#include <vector>
#include <roundOpticalComponent.h>

class RoundFilter : public RoundOpticalComponent
{
private:
    double _minWaveLength;
    double _maxWaveLength;

public:
    /*!
    @param name std::string
    @param position Postion
    @param radius double
    @param minimumWaveLength double
    @param maximumWaveLength double
    */
    RoundFilter(std::string name, Position p, Position n, double r, double min_wl, double max_wl);

    /*!
     Computes the photon direction that it should have after touching the surface of the component.
     @param photon Photon*
     */
    void computePhotonDirection(Photon *photon);
};