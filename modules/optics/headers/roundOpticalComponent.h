#pragma once
#include <opticalComponent.h>
#include <types.h>

class RoundOpticalComponent : public OpticalComponent
{
public:
  double radius;

  /*!
  @param name std::string
  @param position Position
  @param normal Position
  @param radius double
  */
  RoundOpticalComponent(std::string name, Position p, Position n, double r);

  /*!
  Checks wether a photon is inside or outisde of the component surface.
  This check takes consideration of plan surface and radius of the opticalComponent.
  @param photon Photon
  @return bool
  */
  bool isInside(Photon *photon);

  /*!
  Set one parameter of the opticalComponent.
  @param p ParameterName
  @param value double

  ParameterName can be POSITIONX, POSITIONY, POSITIONZ or RADIUS. 
  */
  void setParameter(ParameterName p, double value);

  /*!
  This will take some point from the circle circumference that defines the component border and save their coordinate to a file.
  Those coordinate are needed for the gnuplot visualization of the opticalMachine.
  */
  void saveComponentToFile();

  /*!
  This will return the Picture (std::vector<std::vector<double>>) that the opticalMachine took after running the simulation.
  @param photons std::vector<Photon *>
  @return Picture
  */
  Picture getPicture(std::vector<Photon *> photons);
  
  /*!
  Returns the radius that covers the surface where the photons are supposed to go through.
  @return double
  */
  double getPropagationRadius();
};
