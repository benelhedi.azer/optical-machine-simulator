#pragma once
#include <iostream>
#include <vector>
#include <roundOpticalComponent.h>

class RoundMirror : public RoundOpticalComponent
{
    public:

    /*!
    @param name std::string
    @param postion Position
    @param normal Postion
    @param radius double
    */
    RoundMirror(std::string name, Position p, Position n, double r);
    
    /*!
    Computes the photon direction that it should have after touching the surface of the component.
    @param photon Photon*
    */
    void computePhotonDirection(Photon *photon);
};