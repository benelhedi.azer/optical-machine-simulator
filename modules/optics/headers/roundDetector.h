#pragma once
#include <roundOpticalComponent.h>
#include <types.h>

class RoundDetector : public RoundOpticalComponent
{
private:
  Picture _pic;

public:
  /*!
  @param name std::string
  @param position Position
  @param direction Position
  @param radius double
  */
  RoundDetector(std::string name, Position p, Position n, double r);

   /*!
  Computes the photon direction that it should have after touching the surface of the component.
  @param photon Photon*
  */
  void computePhotonDirection(Photon* photon);
};
