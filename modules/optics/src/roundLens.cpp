#include <roundLens.h>
#include <position.h>
#include <roundOpticalComponent.h>
#include <opticalComponent.h>
#include <tools.h>
#include <iostream>
#include <string>

// NOTE: Normal of the object is supposed to be directing towards the detector/following object, not the previous!

//Constructor
RoundLens::RoundLens(std::string name, Position p, Position n, double r, double fl) : RoundOpticalComponent(name, p, n, r), _fl(fl) {}

// Getters
double RoundLens::getFocalPoint() const
{
	return _fl;
}

void RoundLens::computePhotonDirection(Photon *photon)
{
	Position position = this->getPosition();
	Position normal = this->getNormal();
	if (RoundOpticalComponent::isInside(photon))
	{
		if (_fl >= 0)
		{ // concave lens
			// focal face is defined through normal n and the focal point
			Position ff = (position.operator+(((normal.normalize()).convert<Position>()).operator*(-_fl))).convert<Position>();
			// compute focal point
			Position fp = tools::linePlaneIntersection(ff, position, (normal.normalize()).convert<Position>(), photon->getDirection());
			fp = ((photon->getPosition().convert<Position>()).operator-(fp)).convert<Position>();
			Position new_dir(fp.getX(), fp.getY(), fp.getZ());
			photon->setDirection(new_dir);
		}
		else
		{ // convex lens
			Position ff = (position.operator+(((normal.normalize()).convert<Position>()).operator*(_fl))).convert<Position>();
			// compute focal point
			Position fp = tools::linePlaneIntersection(ff, position, (normal.normalize()).convert<Position>(), ((photon->getDirection()).operator*(-1)).convert<Position>());
			fp = (fp.operator-(photon->getPosition().convert<Position>())).convert<Position>();
			Position new_dir(fp.getX(), fp.getY(), fp.getZ());
			photon->setDirection(new_dir);
		};
	}
	else
	{
		Position new_dir(0, 0, 0);
		photon->setDirection(new_dir);
	};
};

