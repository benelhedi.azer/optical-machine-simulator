#include <photon.h>
#include <position.h>
#include <tools.h>

// Constructor
Photon::Photon(Position p, Position d, double wl, std::string id) : _position(p), _direction(d), _waveLength(wl), Trackable<Position>(id) {
    _addToLog(_position);
}

// Getters
Position Photon::getPosition() const
{
    return _position;
}

Position Photon::getDirection() const
{
    return _direction;
}

double Photon::getWaveLength() const
{
    return _waveLength;
}

// Setters
void Photon::setPosition(Position p)
{
    _position = p;
    _addToLog(p);
}

void Photon::setDirection(Position d)
{
    _direction = d;
}

void Photon::setWaveLength(double wl)
{
    _waveLength = wl;
}

// Public member methods
bool Photon::isInLine(Position p)
{
    double intersectionStep = (p - _position) * (1 / _direction) / 3;
    // std::cout << "intersection step: " << intersectionStep << "\n";
    ThreeD _checkedIntersectionPoint = _position + _direction * intersectionStep;
    Position checkedIntersectionPoint = _checkedIntersectionPoint.convert<Position>();
    // std::cout << "intersection point: " << checkedIntersectionPoint << "\n";

    return p.cmp(checkedIntersectionPoint);
}