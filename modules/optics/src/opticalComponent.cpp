#include <opticalComponent.h>
#include <types.h>
#include <tools.h>
#include <omp.h>

// Constructor
OpticalComponent::OpticalComponent(std::string name, Position p, Position n) : _name(name), _position(p), _normal(n) {}

// Getters
std::string OpticalComponent::getName() const
{
  return _name;
}

Position OpticalComponent::getPosition() const
{
  return _position;
}

Position OpticalComponent::getNormal() const
{
  return _normal;
}

// Setters
void OpticalComponent::setName(std::string name)
{
  _name = name;
}

void OpticalComponent::setPosition(Position p)
{
  _position = p;
}

void OpticalComponent::setNormal(Position n)
{
  _normal = n;
}

void OpticalComponent::computePhotonsPosition(std::vector<Photon *> photons)
{
// OpenMP
#pragma omp parallel for
  for (std::vector<Photon *>::iterator p = std::begin(photons); p != std::end(photons); p++)
  {
    this->propagate(*p);
  }
}

void OpticalComponent::computePhotonsDirection(std::vector<Photon *> photons)
{
// OpenMP
#pragma omp parallel for
  for (std::vector<Photon *>::iterator i = begin(photons); i != end(photons); i++)
  {
    computePhotonDirection(*i);
  }
}

Plane OpticalComponent::getPlane()
{
  return Plane(_normal);
}

void OpticalComponent::propagate(Photon *p)
{
  p->setPosition(tools::linePlaneIntersection(this->getPosition(), p->getPosition(), this->getNormal(), p->getDirection()));
}
