#include <iostream>
#include <vector>
#include <roundMirror.h>
#include <parameterName.enum.h>

RoundMirror::RoundMirror(std::string name, Position p, Position n, double r) : RoundOpticalComponent(name, p, n, r) {}

void RoundMirror::computePhotonDirection(Photon *photon)
{
    ThreeD photonDirection = photon->getDirection();

    if (RoundOpticalComponent::isInside(photon))
    {
        // normalize the normal vector
        ThreeD N = this->getNormal().normalize();

        // calculate vector B => projection of the direction vector onto the normal of the mirror
        double dot = photonDirection.operator*(N);
        ThreeD B = N.operator*(dot);

        // calculate vector A => orthogonal vector to B
        ThreeD A = photonDirection.operator-(B);

        // calculate vector R => new direction vector
        ThreeD R = photonDirection.operator-(B.operator*(2));

        // update photon's direction
        Position dir(R.getX(), R.getY(), R.getZ());
        photon->setDirection(dir);
    }
    else
    {
        Position dir(0, 0, 0);
        photon->setDirection(dir);
    }
}