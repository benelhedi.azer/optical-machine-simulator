#include <position.h>
#include <types.h>
#include <objectiveFunction.h>

ObjectiveFunction::ObjectiveFunction(OpticalMachine *om) : _om(om)
{
    _totalBrightnessWeight = 1.0;
    _focusWeight = 1.0;
}

double ObjectiveFunction::computeTotalBrightness()
{
    // the higher the better
    // the larger the lens the worse & the further it is placed from the original position the worse
    Picture _pic = _om->getPicture();
    int n = _pic.size();
    double totalBrightness = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            totalBrightness += _pic[i][j];
        };
    };
    totalBrightness /= (n ^ 2);
    return totalBrightness;
}

double ObjectiveFunction::computeFocus()
{
    // Computes the average squared difference in brightness between every pixel and its pixel to the right and below.
    // the higher the better
    // the larger the lens the worse & the further it is placed from the original position the worse
    Picture _pic = _om->getPicture();
    int n = _pic.size();
    double contrast = 0;
    double diff;
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - 1; j++)
        {
            diff = (_pic[i][j] - _pic[i][j + 1]) ^ 2;
            diff += (_pic[i][j] - _pic[i + 1][j]) ^ 2;
            contrast += diff / 2;
        };
    };
    for (int i = 0; i < n - 1; i++)
    { // take care of marginal values
        contrast += (_pic[i][n - 1] - _pic[i + 1][n - 1]) ^ 2;
        contrast += (_pic[n - 1][i] - _pic[n - 1][i + 1]) ^ 2;
    };
    double focus = contrast / (n ^ 2);
    return focus;
}

void ObjectiveFunction::setVariable(std::string componentName, ParameterName parameterName)
{
    _componentName = componentName;
    _parameterName = parameterName;
}

double ObjectiveFunction::compute()
{
    return _totalBrightnessWeight * computeTotalBrightness();
}

double ObjectiveFunction::function(double value)
{
    _om->setParameter(_componentName, _parameterName, value);
    _om->resetPhotons();
    _om->run();
    return compute();
}

OpticalMachine *ObjectiveFunction::getOpticalMachine()
{
    return _om;
}