#include <abstractOptimizer.h>
#include <cassert>

AbstractOptimizer::AbstractOptimizer(double left, double right, double h, double learningRate, std::function<double(double)> function) : _left(left), _right(right), _h(h), _learningRate(learningRate), _function(function)
{
    _checkInterval();
}

void AbstractOptimizer::_checkInterval()
{
    assert(_right >= _left && "Wrong interval values for optimizer");
}