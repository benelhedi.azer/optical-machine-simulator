#include <gradientDecentOptimizer.h>
#include <algorithm>
#include <cassert>
#include <limits>
#include <math.h>

GradientDecentOptimizer::GradientDecentOptimizer(
    double left,
    double right,
    double h,
    double learningRate,
    std::function<double(double)> &&function,
    double accuracy) : AbstractOptimizer(left, right, h, learningRate, function), _accuracy(accuracy) {}

double GradientDecentOptimizer::_df(double x)
{
    return (_function(x + _h) - _function(x - _h)) / 2 * _h;
}

double GradientDecentOptimizer::run()
{
    double x = (_left + _right) / 2;
    double step;
    // double learningRate = pow(10, -1);
    double derivative;
    static int iteration = 0;

    do
    {

        derivative = _df(x);
        step = _learningRate * derivative;

        std::cout <<  "[" << iteration++ << "] " << "gradient descent: " << x << ": derivative: " << derivative << std::endl;

        x += step;

        // the loop will stop when the needed accuracy is reached
        // or when the optimization point reaches the border of the given interval.

    } while (abs(derivative) > _accuracy && (x - _accuracy) > _left && (x + _accuracy) < _right);

    return x;
}