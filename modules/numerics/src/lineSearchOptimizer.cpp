#include <lineSearchOptimizer.h>
#include <abstractOptimizer.h>

LineSearchOptimizer::LineSearchOptimizer(double left, double right, double h, double learningRate, std::function<double(double)> function) : AbstractOptimizer(left, right, h, learningRate, function) {}

bool LineSearchOptimizer::_isMaximumRegion(double fx1, double fx2, double fx3)
{
    return fx1 < fx2 && fx3 < fx2;
}

bool LineSearchOptimizer::_isInsideInterval(double x)
{
    return (x - _h > _left) && (x + _h < _right);
}

double LineSearchOptimizer::run()
{
    double x2 = (_left + _right) / 2;
    double x1 = x2 - _h;
    double x3 = x2 + _h;

    double fx1, fx2, fx3;
    double iteration = 0;
    
    do
    {

        fx1 = _function(x1);
        fx2 = _function(x2);
        fx3 = _function(x3);

        double step = _learningRate * 1 / (fx3 - fx1);

        x2 += step;
        double x1 = x2 - _h;
        double x3 = x2 + _h;

        std::cout <<  "[" << iteration++ << "] "  << "line search: " << x2
                  << // " [x1. x2, x3]: [" << x1 << "," << x2 << "," << x3 << "]" <<
            std::endl;

    } while (!_isMaximumRegion(fx1, fx2, fx3) && _isInsideInterval(x2));

    return x2;
}