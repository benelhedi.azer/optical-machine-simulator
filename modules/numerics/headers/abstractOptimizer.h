#pragma once
#include <functional>

class AbstractOptimizer
{

protected:
    double _left;
    double _right;
    double _h;
    double _learningRate;
    std::function<double(double)> _function;

public:
    AbstractOptimizer(double left, double right, double h, double learningRate, std::function<double(double)> function);
    
    /*!
    Runs the optimization algorithm in the handed function until it converges to a an acceptable value.
    */
    virtual double run() = 0;

protected:
    /*!
    Checks whether the given interval (through constructor) is correct or not. 
    _right should always bigger than _left
    */
    void _checkInterval();
};