#pragma once
#include <iostream>
#include <functional>
#include <abstractOptimizer.h>

class LineSearchOptimizer : public AbstractOptimizer
{
public:
    LineSearchOptimizer(double left, double right, double h, double learningRate, std::function<double(double)> function);
    double run();

private:

    /*!
    Checks wether x2 is bigger than both x1 qand x3. 
    In yes x2 is considered a possible maxima.
    @param x1 double
    @param x2 double
    @param x3 double
    */
    bool _isMaximumRegion(double x1, double x2, double x3);

    /*!
    In every step the optimizer guess a new point.
    This function checks whether a point is inside the given interva (_left and _right)
    @param x double
    @return double
    */
    bool _isInsideInterval(double x);
};