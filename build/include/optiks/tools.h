#pragma once
#include <position.h>
#include <formatter.h>
#include <types.h>

namespace tools
{
    /*!
    Finds the intersection point between a plane and a line.
    @param Postion planePositon
    @param Position linePostion
    @param Position normal of the plan
    @param Position line direction vector
    */
    Position linePlaneIntersection(Position planePosition, Position linePosition, Position normal, Position lineDirection);

    /*!
    Configues the Formatter for a specific output format.
    @param int width of each output string
    @param  string should contain three characters in the following order: openingWrapper, separator, closingWrapper. e.g. "[,]" or "{|}"
    */
    Formatter format(int w, std::string s);

    /*!
    Configues the Formatter for a specific output format.
    @param int width of each output string
    @param  string openingWrapper
    @param string separator
    @param string closingWrapper
    */
    Formatter format(int w, std::string ow, std::string s, std::string cw);

    /*!
    Splits a string into a vector of string.
    @param str string
    @param separator string
    */
    StringVector split(std::string str, std::string separtor);

    /*!
    Converts a string into a double (if possible)
    It wrapped std::stod() function but it also supports both comma and dot floating points.
    @param str string 
    */
    double stringToDouble(std::string);
}