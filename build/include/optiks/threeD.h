#pragma once
#include <iostream>

class ThreeD
{
  double _x, _y, _z;

public:
  // Constructor(s)
  ThreeD(double x, double y, double z);
  ThreeD();

  // Getters
  double getX() const;
  double getY() const;
  double getZ() const;

  // Setters
  void setX(double);
  void setY(double);
  void setZ(double);

  // Overloded member methods
  ThreeD operator+(double);
  ThreeD operator-(double);
  ThreeD operator*(double);
  ThreeD operator-();
  ThreeD operator=(ThreeD);
  ThreeD operator+(ThreeD);
  ThreeD operator-(ThreeD);
  double operator*(ThreeD);

  // Overloded friends methods
  friend std::ostream &operator<<(std::ostream &os, const ThreeD &a);
  friend ThreeD operator/(double n, ThreeD a);

  // Public member methods
  ThreeD elementWiseProduct(ThreeD a);
  ThreeD crossProdcut(ThreeD b);
  double norm();
  ThreeD normalize();
  ThreeD normal();
  bool cmp(ThreeD a);

  virtual bool isValid(double x, double y, double z);

  template <class T>
  T convert();
};
