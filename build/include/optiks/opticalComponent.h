#pragma once
#include <iostream>
#include <position.h>
#include <angle.h>
#include <photon.h>
#include <vector>
#include <plane.h>
#include <parameterName.enum.h>
#include <photon.h>

typedef std::vector<std::vector<int>> Picture;

class Photon;

class OpticalComponent
{
  std::string _name;
  Position _position;
  Position _normal;

public:
  // Constructor
  OpticalComponent(std::string name, Position p, Position n);

  // Getters
  std::string getName() const;
  Position getPosition() const;
  Position getNormal() const;

  // Setters
  void setName(std::string);
  void setPosition(Position);
  void setNormal(Position);

  /*!
  Calculates the direction of one given photon
  The result should be directly saved/overwrites photon->direction
  This is a pure virtual function and must be implemented in every OpticalComponent child class
  @param photon Photon*
  */
  virtual void computePhotonDirection(Photon *photon) = 0;

  /*!
  Loops over all photons in vector<Photon *> and calls opticaComponent.computePhotonDirection(Photon) for every for-loop iteration
  Preferably optimized with openMP
  @param photons std::vector<Photon *>
  */
  void computePhotonsDirection(std::vector<Photon *> photons);

  /*!
  Preferably optimized with openMP
  @param photons std::vector<Photon *>
  */
  void computePhotonsPosition(std::vector<Photon *> photons);

  /*!
  Calculates whether or not the photon lies within the optical component after computing its intersection.
  This is a pure virtual function and must be implemented in every direct child class of opticalComponent.
  @param photon Photon*
 */
  virtual bool isInside(Photon *photon) = 0;

  /*!
  Set parameter of opticalComponent
  @param opticalCompoenntPArameterName ParameterName
  @param value double
  */
  virtual void setParameter(ParameterName p, double value) = 0;

  /*!
  moves photon from one component to the next following it's precalculated direction.
  @param photon Photon*
  */
  void propagate(Photon *p);

  /*!
  Depends on the component shape this function will save it's shape geometry into a file that will later be used for gnuplot visualization.
  */
  virtual void saveComponentToFile() = 0;

  /*!
  Returns the plane where the opticalComponent lays.
  The plan will be computed according to the normal vector of the opticalComponent.
  */
  Plane getPlane();

  /*!
  This will return the Picture (std::vector<std::vector<double>>) that the opticalMachine took after running the simulation.
  @param photons std::vector<Photon *>
  @return Picture
  */
  virtual Picture getPicture(std::vector<Photon *> photons) = 0;

  /*!
  Returns the radius that covers the surface where the photons are supposed to go through.
  @return double
  */
  virtual double getPropagationRadius() = 0;
};
