#pragma once
#include <threeD.h>

class Angle : public ThreeD
{
  public:
  Angle(double x, double y, double z);
  bool isValid(double x, double y, double z);
};