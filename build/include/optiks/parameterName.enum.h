#pragma once

enum ParameterName
{
  POSITIONX,
  POSITIONY,
  POSITIONZ,
  RADIUS
};