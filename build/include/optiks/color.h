#pragma once
#include <threeD.h>

class Color : public ThreeD
{
  /*
  Private member method
  Returns the equivalent wavelength of this color object  
  */
  double convert();
  
  public:
  Color(double x, double y, double z);
  bool isValid(double x, double y, double z);
};