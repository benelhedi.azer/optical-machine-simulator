#pragma once
#include <iostream>

class Formatter
{
public:
    // Public member varaible(s)
    static int width;
    static std::string openWrapper;
    static std::string seperator;
    static std::string closeWrapper;

    // Constructor(s)
    Formatter();

    // Public member method(s)
    static Formatter set(int, std::string, std::string, std::string);

    // Overloaded friend method(s)
    friend std::ostream &operator<<(std::ostream &os, const Formatter &a);
};
